# mdmathjaxtests

Purpose
-------

* To experiment with MathJax and Markdown on Codeberg
* <div>$x^2+y^2=1$</div>
* [Linked content](linked.html)
* [Codeberg MarkDown documentation](https://docs.codeberg.org/markdown/)
* [Co-product page](diagrams/coprod.html)
* [This project's page](https://yhardy.codeberg.page/mdmathjaxtests/)


AMSCD diagram
-------------

<div>$$\begin{CD} A @>>> & B \end{CD}$$</div>

XY-Pic diagram (only works when viewing the pages directly)
--------------

<div>
\begin{xy}
 \xymatrix{ A \ar[r] & B }
\end{xy}
</div>
