.SUFFIXES:	.html .md

HEADER=	\
<script> \
 MathJax = { \
  loader: { \
   load: ['[custom]/xypic.js'], \
   paths: {custom: 'https://cdn.jsdelivr.net/gh/sonoisa/XyJax-v3@3.0.1/build/'} \
  }, \
  tex: { \
   inlineMath: [['$$', '$$'], ['\\\\(', '\\\\)']], \
   packages: {'[+]': ['xypic']} \
  } \
 }; \
</script> \
<script type=\"text/javascript\" id=\"MathJax-script\" async src=\"https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js\"></script>

default:	pages
pages:		index.html
pages:		linked.html
pages:		diagrams/coprod.html
index.html:	README.md
	mkd2html -header "${HEADER}" README.md index.html

.md.html:
	mkd2html -header "${HEADER}" $< $@
